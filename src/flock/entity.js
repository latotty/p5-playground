import { pipe } from './util';
import { vec, vecAdd, vecSub, vecDiv, vecMult, vecLimit, vecSetMag, vecRandom } from './p5';

export const createEntity = ({ width, height }) => {
  return {
    pos: vec(random(width), random(height)),
    vel: vecSetMag(vecRandom(), random(2, 4)),
    acc: vec(),
  };
};

export const updateEntity = ({
  width,
  height,
  maxSpeed,
  maxForce,
  alignForce,
  cohesionForce,
  separationForce,
  localFlock,
  entity,
}) => {
  return pipe(
    updateEntityEdge({ width, height }),
    updateEntityAlign({ localFlock, maxSpeed, maxForce, alignForce }),
    updateEntityCohesion({ localFlock, maxSpeed, maxForce, cohesionForce }),
    updateEntitySeparation({ localFlock, maxSpeed, maxForce, separationForce }),
    updateEntityVel({ maxSpeed }),
    updateEntityPos,
  )(entity);
};

const updateEntitySeparation = ({ localFlock, maxSpeed, maxForce, separationForce }) => entity => {
  if (!localFlock.length) {
    return entity;
  }

  const separationVector = vecLimit(
    vecSub(
      vecSetMag(
        vecDiv(
          localFlock.reduce((acc, e) => vecAdd(acc, vecDiv(vecSub(entity.pos, e.pos), e.dist ** 2)), vec()),
          localFlock.length,
        ),
        maxSpeed,
      ),
      entity.vel,
    ),
    maxForce,
  );

  return {
    ...entity,
    acc: vecAdd(entity.acc, vecMult(separationVector, separationForce)),
  };
};

const updateEntityCohesion = ({ localFlock, maxSpeed, maxForce, cohesionForce }) => entity => {
  if (!localFlock.length) {
    return entity;
  }

  const cohesionVector = vecLimit(
    vecSub(
      vecSetMag(
        vecSub(vecDiv(localFlock.reduce((acc, e) => vecAdd(acc, e.pos), vec()), localFlock.length), entity.pos),
        maxSpeed,
      ),
      entity.vel,
    ),
    maxForce,
  );

  return {
    ...entity,
    acc: vecAdd(entity.acc, vecMult(cohesionVector, cohesionForce)),
  };
};

const updateEntityAlign = ({ localFlock, maxSpeed, maxForce, alignForce }) => entity => {
  if (!localFlock.length) {
    return entity;
  }

  const alignVector = vecLimit(
    vecSub(
      vecSetMag(vecDiv(localFlock.reduce((acc, e) => vecAdd(acc, e.vel), vec()), localFlock.length), maxSpeed),
      entity.vel,
    ),
    maxForce,
  );

  return {
    ...entity,
    acc: vecAdd(entity.acc, vecMult(alignVector, alignForce)),
  };
};

const updateEntityEdge = ({ width, height }) => entity => ({
  ...entity,
  pos:
    entity.pos.x < 0
      ? vec(width, entity.pos.y)
      : entity.pos.x > width
      ? vec(0, entity.pos.y)
      : entity.pos.y < 0
      ? vec(entity.pos.x, height)
      : entity.pos.y > height
      ? vec(entity.pos.x, 0)
      : entity.pos,
});

const updateEntityPos = entity => ({
  ...entity,
  pos: vecAdd(entity.pos, entity.vel),
});

const updateEntityVel = ({ maxSpeed }) => entity => ({
  ...entity,
  vel: vecAdd(entity.vel, entity.acc).limit(maxSpeed),
  acc: vec(),
});

export const drawEntity = ({ viewRange, viewAngle, drawVision }) => entity => {
  push();
  stroke(255);
  strokeWeight(2);
  fill(255);
  translate(entity.pos.x, entity.pos.y);
  rotate(entity.vel.heading());
  let arrowSize = 4;
  triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
  if (drawVision) {
    stroke(255, 100);
    strokeWeight(1);
    fill(100, 100, 100, 100);
    if (viewAngle >= 360) {
      circle(0, 0, viewRange / 2);
    } else {
      arc(0, 0, viewRange, viewRange, radians(viewAngle / -2), radians(viewAngle / 2));
    }
  }
  pop();
};
