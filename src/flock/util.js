export const pipe = (...fns) => arg => fns.reduce((acc, fn) => fn(acc), arg);
