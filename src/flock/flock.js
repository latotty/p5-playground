import { updateEntity } from './entity';
import { vecAdd, vecDist, vecSub, vecAngleBetween } from './p5';

export const updateFlock = ({
  width,
  height,
  maxSpeed,
  maxForce,
  viewRange,
  viewAngle,
  alignForce,
  cohesionForce,
  separationForce,
}) => state => {
  const translateMap = getTranslateMap({ width, height });
  const halfViewAngle = viewAngle / 2;
  return {
    ...state,
    flock: state.flock.map(entity =>
      updateEntity({
        entity,
        width,
        height,
        maxSpeed,
        maxForce,
        alignForce,
        cohesionForce,
        separationForce,
        localFlock: getLocalFlock({
          entity,
          translateMap,
          viewRange,
          halfViewAngle,
          flock: state.flock,
        }),
      }),
    ),
  };
};

const getTranslateMap = ({ width, height }) => [
  [0, 0],
  [0, height * -1],
  [0, height],
  [width, 0],
  [width * -1, 0],
  [width, height * -1],
  [width, height],
  [width * -1, height * -1],
  [width * -1, height],
];

const getLocalFlock = ({ flock, entity, viewRange, halfViewAngle, translateMap }) => {
  return flock
    .filter(e => e !== entity)
    .flatMap(e => {
      for (const trans of translateMap) {
        const newPos = vecAdd(e.pos, trans);

        const angle = vecAngleBetween(entity.vel, vecSub(newPos, entity.pos));
        if (angle > halfViewAngle) {
          continue;
        }

        const dist = vecDist(newPos, entity.pos);
        if (dist < viewRange) {
          return [
            {
              ...e,
              pos: newPos,
              dist,
            },
          ];
        }
      }
      return [];
    });
};
