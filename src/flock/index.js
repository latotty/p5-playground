import { createEntity, drawEntity } from './entity';
import { updateFlock } from './flock';

let state = {
  flock: [],
};

let drawVisionCheckbox;
let flockSizeSlider;
let viewRangeSlider;
let viewAngleSlider;
let maxSpeedSlider;
let maxForceSlider;
let alignForceSlider;
let cohesionForceSlider;
let separationForceSlider;

const createTextSlider = ({ label, min, max, value, step }) => {
  const mainDiv = createDiv();
  mainDiv.style('display: inline-block; padding: 5px;');
  const textDiv = createDiv(`${label}: `);
  textDiv.parent(mainDiv);

  const textSpan = createSpan(value);
  textSpan.parent(textDiv);

  const sliderDiv = createDiv();
  sliderDiv.parent(mainDiv);

  const slider = createSlider(min, max, value, step);
  slider.parent(sliderDiv);

  let lastValue = value;

  return {
    getValue: () => slider.value(),
    setValue: val => slider.value(val),
    update: onChange => {
      const newValue = slider.value();
      if (newValue !== lastValue) {
        textSpan.html(slider.value());
        onChange && onChange(newValue, lastValue);
        lastValue = newValue;
        return true;
      }
      return false;
    },
  };
};

window.setup = () => {
  createCanvas(640, 640);

  drawVisionCheckbox = createCheckbox('drawVision', false);

  flockSizeSlider = createTextSlider({ label: 'FlockSize', min: 0, max: 1000, value: 100, step: 10 });
  viewRangeSlider = createTextSlider({ label: 'ViewRange', min: 0, max: 1000, value: 50, step: 1 });
  viewAngleSlider = createTextSlider({ label: 'ViewAngle', min: 0, max: 360, value: 220, step: 1 });
  maxSpeedSlider = createTextSlider({ label: 'MaxSpeed', min: 0, max: 10, value: 4, step: 0.1 });
  maxForceSlider = createTextSlider({ label: 'MaxForce', min: 0, max: 1, value: 0.2, step: 0.01 });

  alignForceSlider = createTextSlider({ label: 'AlignForce', min: 0, max: 1, value: 0.7, step: 0.01 });
  cohesionForceSlider = createTextSlider({ label: 'CohesionForce', min: 0, max: 1, value: 0.4, step: 0.01 });
  separationForceSlider = createTextSlider({ label: 'SeparationForce', min: 0, max: 1, value: 0.8, step: 0.01 });

  state = reset({ width, height, flockSize: 100 });
};

window.draw = () => {
  background(34);

  flockSizeSlider.update(value => (state = reset({ width, height, flockSize: value })));
  viewRangeSlider.update();
  viewAngleSlider.update();
  maxSpeedSlider.update();
  maxForceSlider.update();
  alignForceSlider.update();
  cohesionForceSlider.update();
  separationForceSlider.update();

  const viewRange = viewRangeSlider.getValue();
  const viewAngle = viewAngleSlider.getValue();
  const maxSpeed = maxSpeedSlider.getValue();
  const maxForce = maxForceSlider.getValue();

  const alignForce = alignForceSlider.getValue();
  const cohesionForce = cohesionForceSlider.getValue();
  const separationForce = separationForceSlider.getValue();

  state = updateFlock({
    width,
    height,
    maxSpeed,
    maxForce,
    viewRange,
    viewAngle,
    alignForce,
    cohesionForce,
    separationForce,
  })(state);

  state.flock.forEach(drawEntity({ viewRange, viewAngle, drawVision: drawVisionCheckbox.checked() }));
};

const reset = ({ width, height, flockSize }) => ({
  flock: Array(flockSize)
    .fill()
    .map(() => createEntity({ width, height })),
});

new p5();
