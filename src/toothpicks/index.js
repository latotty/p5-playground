const TOOTHPICK_LENGTH = 35;
const TOOTHPICK_LENGTH_HALF = (TOOTHPICK_LENGTH - 1) / 2;

let toothpicks = [];
let newToothpicks = [];
let scaleNum = 1;

window.setup = () => {
  createCanvas(640, 640);
  setFrameRate(1);

  newToothpicks = [{ x: 0, y: 0, align: false }];
};

window.draw = () => {
  background(255);
  translate(width / 2, height / 2);
  scale(scaleNum);

  toothpicks.forEach(drawToothpick(false));
  newToothpicks.forEach(drawToothpick(true));

  toothpicks = [...toothpicks, ...newToothpicks];
  newToothpicks = generateToothpicks(toothpicks, newToothpicks);

  const leftBound = width / 2 / scaleNum * -1;
  const rightBound = width / 2 / scaleNum;
  const topBound = height / 2 / scaleNum * -1;
  const bottomBound = height / 2 / scaleNum;
  if (newToothpicks.some(({ x, y }) => x < leftBound || x > rightBound || y < topBound || y > bottomBound)) {
    scaleNum /= 2;
  }
};

window.keyPressed = () => {
  if (keyCode === 32) { // SPACE
    redraw();
  }
}

const getToothpickCoords = t =>
  t.align
    ? [[t.x, t.y - TOOTHPICK_LENGTH_HALF], [t.x, t.y + TOOTHPICK_LENGTH_HALF]]
    : [[t.x - TOOTHPICK_LENGTH_HALF, t.y], [t.x + TOOTHPICK_LENGTH_HALF, t.y]];

const generateToothpicks = (allToothpicks, newToothpicks) => {
  return newToothpicks.flatMap(t => {
    let okA = true;
    let okB = true;
    const [tCoordsA, tCoordsB] = getToothpickCoords(t);
    for (const t2 of allToothpicks) {
      if (t === t2) {
        continue;
      }
      const [t2CoordsA, t2CoordsB] = getToothpickCoords(t2);
      if (
        okA &&
        ((tCoordsA[0] === t2CoordsA[0] && tCoordsA[1] === t2CoordsA[1]) ||
          (tCoordsA[0] === t2CoordsB[0] && tCoordsA[1] === t2CoordsB[1]))
      ) {
        okA = false;
      }
      if (
        okB &&
        ((tCoordsB[0] === t2CoordsA[0] && tCoordsB[1] === t2CoordsA[1]) ||
          (tCoordsB[0] === t2CoordsB[0] && tCoordsB[1] === t2CoordsB[1]))
      ) {
        okB = false;
      }
      if (!okA && !okB) {
        break;
      }
    }
    return [
      okA && { x: tCoordsA[0], y: tCoordsA[1], align: !t.align },
      okB && { x: tCoordsB[0], y: tCoordsB[1], align: !t.align },
    ].filter(Boolean);
  });
};

const drawToothpick = newColor => toothpick => {
  push();
  translate(toothpick.x, toothpick.y);
  if (newColor) {
    stroke(255, 0, 0);
  } else {
    stroke(0);
  }
  strokeWeight(3);
  if (toothpick.align) {
    line(0, TOOTHPICK_LENGTH_HALF * -1, 0, TOOTHPICK_LENGTH_HALF);
  } else {
    line(TOOTHPICK_LENGTH_HALF * -1, 0, TOOTHPICK_LENGTH_HALF, 0);
  }
  pop();
};

new p5();
